# 1.8 Telefonbuch

**Voraussetzung**:
Vorlesung zum Thema Maps.

**Ziel**:
Kennenlernen der API von `Map` und `SortedMap` durch einen Reverse-Engineering-Ansatz, bei dem eine Java-API-Dokumentation als Pflichtenheft verwendet wird.

**Dauer**:
< 1 Stunde

## Aufgabenstellung
Erstellen Sie eine Klasse `PhoneBook` (dt. Telefonbuch), die für die Verwaltung von Kontakteinträgen zuständig ist. Halten Sie sich bei der Implementierung der Klasse streng an die bereitgestellte JavaDoc. 

### Schritte zur Umsetzung
1. **JavaDoc öffnen:** Die JavaDoc finden Sie unter dem oben genannten Link. Laden Sie das Projekt als ZIP-Datei herunter und entpacken Sie es lokal. Im Verzeichnis `doc` finden Sie eine Datei `index.html`, die Sie mit einem Browser öffnen können.

2. **Dokumentation als Pflichtenheft:** Die Dokumentation enthält genaue Angaben zu Klassen, Methoden und Attributen. Diese Dokumentation muss als Pflichtenheft verstanden werden, das 1:1 umgesetzt werden soll.

3. **Implementierung der Klasse `PhoneBook`:** 
   - Entwickeln Sie die Klasse basierend auf den Vorgaben der JavaDoc.
   - Achten Sie darauf, alle beschriebenen Klassen, Methoden und Attribute exakt wie angegeben zu implementieren.

4. **Überprüfung:** Stellen Sie sicher, dass Ihre Implementierung mit der Dokumentation übereinstimmt und die geforderten Funktionalitäten korrekt umgesetzt sind.